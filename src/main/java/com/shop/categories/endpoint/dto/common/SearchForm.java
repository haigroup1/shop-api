package com.shop.categories.endpoint.dto.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchForm {

//    private String status;
    private String loai;
    private String maHTGui;
    private String maHTNhan;
    private Date fromDate;
    private Date toDate;
    private String type_request;
    private String maHSGui;
    private String maHSNhan;
}


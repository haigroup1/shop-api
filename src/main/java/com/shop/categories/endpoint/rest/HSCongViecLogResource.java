package com.shop.categories.endpoint.rest;


import com.shop.categories.auth.dto.common.JsonHelper;
import com.shop.categories.auth.dto.common.ResponseData;
import com.shop.categories.common.constants.Constants;
import com.shop.categories.dao.entity.HSCongViecLog;
import com.shop.categories.dao.repository.HSCongViecLogRepository;
import com.shop.categories.endpoint.dto.common.SearchForm;
import com.shop.categories.dao.entity.dto.HSCongViecDateSearch;
import com.shop.categories.dao.entity.dto.HSCongViecLogSearchObject;
import com.shop.categories.dao.entity.dto.LogDataDoiSoatDTO;
import com.shop.categories.dao.entity.dto.TanSuatTheoNgay;
import com.shop.categories.service.HSCongViecLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@ComponentScan("com.savis.categories.endpoint.rest")
@RequestMapping("/savis/categories/api/v1/log")
public class HSCongViecLogResource {

    @Autowired
    HSCongViecLogService hsCongViecLogService;

    @Autowired
    HSCongViecLogRepository hsCongViecLogRepository;


    @RequestMapping(value = "/listLogRequest", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<Page<HSCongViecLog>> getPageLog( @RequestParam(required = false) String search, Pageable pageable) {
        ResponseData<Page<HSCongViecLog>> response = new ResponseData<>();
        HSCongViecLogSearchObject searchObject = JsonHelper.jsonToObject(search, HSCongViecLogSearchObject.class);
        Page<HSCongViecLog> hoSoPage =null ;
        try {
            hoSoPage = hsCongViecLogService.getPageSaveLog(searchObject, pageable);
            response.setData((Page<HSCongViecLog>) hoSoPage);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "/amountLog", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<List<TanSuatTheoNgay>> countLog( @RequestParam(required = false) String search, Pageable pageable) {
        ResponseData<List<TanSuatTheoNgay>> response = new ResponseData<>();
        HSCongViecLogSearchObject searchObject = JsonHelper.jsonToObject(search, HSCongViecLogSearchObject.class);
        List<TanSuatTheoNgay> hoSoAmount =null ;
        try {
            hoSoAmount = hsCongViecLogService.countLog(searchObject);
            response.setData( hoSoAmount);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "/amountLogPage", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<Page<TanSuatTheoNgay>> countLogPage( @RequestParam(required = false) String search, Pageable pageable) {
        ResponseData<Page<TanSuatTheoNgay>> response = new ResponseData<>();
        HSCongViecLogSearchObject searchObject = JsonHelper.jsonToObject(search, HSCongViecLogSearchObject.class);
        Page<TanSuatTheoNgay> hoSoAmount =null ;
        try {
            hoSoAmount = hsCongViecLogService.countLogPage(searchObject, pageable);
            response.setData( hoSoAmount);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "/amountLog/listLogByDate", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<Page<LogDataDoiSoatDTO>> getPageLogFollowDate( @RequestParam(required = false) String search,
                                                                @RequestParam(required = false) String type, Pageable pageable) {
        ResponseData<Page<LogDataDoiSoatDTO>> response = new ResponseData<>();
        HSCongViecDateSearch searchObject = JsonHelper.jsonToObject(search, HSCongViecDateSearch.class);
        Page<LogDataDoiSoatDTO> hoSoPage =null ;
        try {
            hoSoPage = hsCongViecLogService.getPageGetLogFollowDate(searchObject, type, pageable);
            response.setData((Page<LogDataDoiSoatDTO>) hoSoPage);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<Page<HSCongViecLog>> getPage( @RequestParam(required = false) String search, Pageable pageable) {
        ResponseData<Page<HSCongViecLog>> response = new ResponseData<>();
        SearchForm searchObject = JsonHelper.jsonToObject(search, SearchForm.class);
        Page<HSCongViecLog> hoSoAmount =null ;
        try {
            hoSoAmount = hsCongViecLogService.getPageGetLog(searchObject, pageable);
            response.setData( hoSoAmount);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public @ResponseBody
    ResponseData<HSCongViecLog> getById( @RequestParam("id") Integer id) {
        ResponseData<HSCongViecLog> response = new ResponseData<>();
        HSCongViecLog hoSoAmount =null ;
        try {
            hoSoAmount = hsCongViecLogRepository.findOne(id);
            response.setData( hoSoAmount);
            response.setCode(Constants.SUCCESS_CODE);
            response.setMessage(Constants.SUCCESS_MSG);

        } catch (Exception e) {
            e.printStackTrace();
            response.setData(null);
            response.setCode(Constants.ERR_CODE_BAD_REQUEST);
            response.setMessage(Constants.MSG_TEMP + Constants.ERR_MSG_BAD_REQUEST);
        }
        return response;
    }

}

package com.shop.categories.endpoint.rest;

import com.shop.categories.common.exception.Result;
import com.shop.categories.dao.entity.dto.HSCVSystemSearchDTO;
import com.shop.categories.dao.entity.HSCVSystem;
import com.shop.categories.auth.dto.common.JsonHelper;
import com.shop.categories.auth.dto.common.ResponseData;
import com.shop.categories.service.HSCVSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author
 * @created
 *
 * @modified 07/12/2020
 * @modifier DucPM
 */
@Controller
@ComponentScan("com.savis.categories.endpoint.rest")
@RequestMapping("/savis/categories/api/v1/system")
public class HSCVSystemResource {

    @Autowired
    private HSCVSystemService hscvSystemService;

    @GetMapping(value = "", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<ResponseData<Page<HSCVSystem>>> getPage( Pageable pageable,
                                                                   @RequestParam(value = "search", required = false) String search,
                                                                   @RequestParam(value = "filter", required = false) String filter) {
        ResponseData<Page<HSCVSystem>> response = new ResponseData<>();
        Page<HSCVSystem> pageApi = null;
        HSCVSystemSearchDTO searchObject = JsonHelper.jsonToObject(search, HSCVSystemSearchDTO.class);

        // advance search
        if (searchObject != null) {
            pageApi = hscvSystemService.getPage(pageable, searchObject);
        }
        // filter search
        else if (filter != null) {
            pageApi = hscvSystemService.getPage(pageable, filter);
        }
        // get page default
        else {
            pageApi = hscvSystemService.getPage(pageable);
        }

        if (pageApi != null) {
            response = new ResponseData<Page<HSCVSystem>>(pageApi, Result.SUCCESS);
            return new ResponseEntity<ResponseData<Page<HSCVSystem>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<Page<HSCVSystem>>(pageApi, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<Page<HSCVSystem>>>(response, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/list", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<ResponseData<List<HSCVSystem>>> getList() {
        ResponseData<List<HSCVSystem>> response = new ResponseData<>();
        List<HSCVSystem> listApi = null;
        listApi = hscvSystemService.getList();
        if (listApi != null) {
            response = new ResponseData<List<HSCVSystem>>(listApi, Result.SUCCESS);
            return new ResponseEntity<ResponseData<List<HSCVSystem>>>(response, HttpStatus.OK);
        }
        response = new ResponseData<List<HSCVSystem>>(listApi, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<List<HSCVSystem>>>(response, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<ResponseData<HSCVSystem>> findOne(@PathVariable("id") int apiId) {
        ResponseData<HSCVSystem> response = new ResponseData<HSCVSystem>();
        HSCVSystem hscvSystem = hscvSystemService.getOne(apiId);
        if (hscvSystem != null) {
            response = new ResponseData<HSCVSystem>(hscvSystem, Result.SUCCESS);
            return new ResponseEntity<ResponseData<HSCVSystem>>(response, HttpStatus.OK);
        }
        response = new ResponseData<HSCVSystem>(hscvSystem, Result.NO_CONTENT);
        return new ResponseEntity<ResponseData<HSCVSystem>>(response, HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<ResponseData<Integer>> create(@RequestBody HSCVSystem hscvSystem) {
        Result result = hscvSystemService.create(hscvSystem);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.CREATED);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<ResponseData<Integer>> update(@RequestBody HSCVSystem hscvSystem) {
        //finbyid HSCVSystem
        //set lại các trường cần chỉnh sửa
        //truyền
        Result result = hscvSystemService.update(hscvSystem);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.OK);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseData<Integer>> delete(@PathVariable("id") Integer id) {
        Result result = hscvSystemService.delete(id);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.OK);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseData<Integer>> deleteAllBatch(@RequestParam("entityIds") int[] entityIds) {
        Result result = hscvSystemService.delete(entityIds);
        if (result.isSuccess()) {
            return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.OK);
        }
        return new ResponseEntity<ResponseData<Integer>>(new ResponseData<>(result), HttpStatus.BAD_REQUEST);
    }

}

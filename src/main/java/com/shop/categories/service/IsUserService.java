package com.shop.categories.service;

import com.shop.categories.auth.dto.user.AccessTokenInfo;
import com.shop.categories.auth.dto.user.UserInfo;

public interface IsUserService {

	UserInfo getUserInfo(String authorizationCode);

	AccessTokenInfo getAccessToken(String refreshToken);

}

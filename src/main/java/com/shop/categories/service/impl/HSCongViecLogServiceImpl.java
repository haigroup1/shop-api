package com.shop.categories.service.impl;

import com.shop.categories.dao.entity.HSCongViecLog;
import com.shop.categories.dao.entity.dto.HSCongViecDateSearch;
import com.shop.categories.dao.entity.dto.HSCongViecLogSearchObject;
import com.shop.categories.dao.entity.dto.LogDataDoiSoatDTO;
import com.shop.categories.dao.entity.dto.TanSuatTheoNgay;
import com.shop.categories.dao.repository.HSCongViecLogRepository;
import com.shop.categories.dao.specifications.HSCongViecLogSpecification;
import com.shop.categories.endpoint.dto.common.SearchForm;
import com.shop.categories.service.HSCongViecLogService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.text.SimpleDateFormat;
import java.util.List;



@Service
public class HSCongViecLogServiceImpl implements HSCongViecLogService {

    @Autowired
    HSCongViecLogRepository hsCongViecLogRepository;

    private Logger log = Logger.getLogger(HSCVSystemServiceImpl.class);

    @Autowired
    private EntityManager entityManager;


    @Override
    public List<TanSuatTheoNgay> countLog ( HSCongViecLogSearchObject hsCongViecLogSearchObject ) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

            String query = "select CONVERT(DATE,creation_date) as ngay, COUNT(profile_id) as so_luong from hscv_log_data WHERE ";
            if(hsCongViecLogSearchObject.getLoaiDoiSoat() != null) {
                if(hsCongViecLogSearchObject.getLoaiDoiSoat().equals("SEND")) {
                    query += " type_request = 'SAVE'";
                    if(hsCongViecLogSearchObject.getHeThong() != null) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            query += " AND ma_ht_gui like '%"+hsCongViecLogSearchObject.getHeThong()+"%'";
                        }
                    }
                } else if(hsCongViecLogSearchObject.getLoaiDoiSoat().equals("RECEIVED")) {
                    query += " type_request = 'GET'";
                    if(hsCongViecLogSearchObject.getHeThong() != null) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            query += " AND ma_ht_nhan like '%"+hsCongViecLogSearchObject.getHeThong()+"%'";
                        }
                    }
                } else {
                    query += " type_request = ''";
                    if(hsCongViecLogSearchObject.getHeThong() != null) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            query += " AND ma_ht_gui like '%"+hsCongViecLogSearchObject.getHeThong()+"%'";
                        }
                    }
                }
            } else {
                query += " type_request = ''";
            }
            if(hsCongViecLogSearchObject.getLoai() != null) {
                if(!hsCongViecLogSearchObject.getLoai().isEmpty()) {
                    query += " AND loai = '"+hsCongViecLogSearchObject.getLoai()+"'";
                }
            }
            if(hsCongViecLogSearchObject.getToDate() != null ){
                query += " AND creation_date <= '"+formatter.format(hsCongViecLogSearchObject.getToDate())+"'";
            }
            if(hsCongViecLogSearchObject.getFromDate() != null ){
                query += " AND creation_date >= '"+formatter.format(hsCongViecLogSearchObject.getFromDate())+"'";
            }


            query += " GROUP BY CONVERT(DATE,creation_date)";

            List<TanSuatTheoNgay> tanSuatTheoNgays = entityManager.createNativeQuery(query).getResultList();

            return tanSuatTheoNgays;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public Page<TanSuatTheoNgay> countLogPage ( HSCongViecLogSearchObject hsCongViecLogSearchObject, Pageable pageable ) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Page<TanSuatTheoNgay> tanSuatTheoNgayPage = null;

            String mainQuery = "SELECT table_tong.ngay, table_tong.so_luong, table_thanh_cong.so_luong_tc\n" +
                    "FROM \n" +
                    "( ";

            String queryOne = "select CONVERT(DATE,creation_date) as ngay, COUNT(id) as so_luong from hscv_log_data WHERE ";
            String queryTwo = "select CONVERT(DATE,creation_date) as ngay, COUNT(id) as so_luong_tc from hscv_log_data WHERE ";
            if(hsCongViecLogSearchObject.getLoaiDoiSoat() != null) {
                if(hsCongViecLogSearchObject.getLoaiDoiSoat().equals("SEND")) {
                    queryOne += " type_request = 'SAVE'";
                    queryTwo += " type_request = 'SAVE' AND status_code != 4";
                    if(hsCongViecLogSearchObject.getHeThong() != null) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            queryOne += " AND ma_ht_gui like '%"+hsCongViecLogSearchObject.getHeThong()+"%'";
                            queryTwo += " AND ma_ht_gui like '%"+hsCongViecLogSearchObject.getHeThong()+"%'";
                        }
                    }
                } else if(hsCongViecLogSearchObject.getLoaiDoiSoat().equals("RECEIVED")) {
                    queryOne += " type_request = 'GET'";
                    queryTwo += " type_request = 'GET' AND status_code != 4";
                    if(hsCongViecLogSearchObject.getHeThong() != null) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            queryOne += " AND ma_ht_nhan like '%"+hsCongViecLogSearchObject.getHeThong()+"%'";
                            queryTwo += " AND ma_ht_nhan like '%"+hsCongViecLogSearchObject.getHeThong()+"%'";
                        }
                    }
                } else {
                    queryOne += " type_request = ''";
                    queryTwo += " type_request = '' AND status_code != 4";
                    if(hsCongViecLogSearchObject.getHeThong() != null) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            queryOne += " AND ma_ht_gui like '%"+hsCongViecLogSearchObject.getHeThong()+"%'";
                            queryTwo += " AND ma_ht_gui like '%"+hsCongViecLogSearchObject.getHeThong()+"%'";
                        }
                    }
                }
            } else {
                queryOne += " type_request = ''";
                queryTwo += " type_request = '' AND status_code != 4";
            }
            if(hsCongViecLogSearchObject.getLoai() != null) {
                if(!hsCongViecLogSearchObject.getLoai().isEmpty()) {
                    queryOne += " AND loai = '"+hsCongViecLogSearchObject.getLoai()+"'";
                    queryTwo += " AND loai = '"+hsCongViecLogSearchObject.getLoai()+"'";
                }
            }
            if(hsCongViecLogSearchObject.getToDate() != null ){
                queryOne += " AND creation_date <= '"+formatter.format(hsCongViecLogSearchObject.getToDate())+"'";
                queryTwo += " AND creation_date <= '"+formatter.format(hsCongViecLogSearchObject.getToDate())+"'";
            }
            if(hsCongViecLogSearchObject.getFromDate() != null ){
                queryOne += " AND creation_date >= '"+formatter.format(hsCongViecLogSearchObject.getFromDate())+"'";
                queryTwo += " AND creation_date >= '"+formatter.format(hsCongViecLogSearchObject.getFromDate())+"'";
            }

            if(hsCongViecLogSearchObject.getMa_hs_gui() != null) {
                if(!hsCongViecLogSearchObject.getMa_hs_gui().isEmpty()) {
                    queryOne += " AND ma_hs_gui like '%"+hsCongViecLogSearchObject.getMa_hs_gui()+"%'";
                    queryTwo += " AND ma_hs_gui like '%"+hsCongViecLogSearchObject.getMa_hs_gui()+"%'";
                }
            }

            if(hsCongViecLogSearchObject.getMa_hs_nhan() != null) {
                if(!hsCongViecLogSearchObject.getMa_hs_nhan().isEmpty()) {
                    queryOne += " AND ma_hs_nhan like '%"+hsCongViecLogSearchObject.getMa_hs_nhan()+"%'";
                    queryTwo += " AND ma_hs_nhan like '%"+hsCongViecLogSearchObject.getMa_hs_nhan()+"%'";
                }
            }

            queryOne += " GROUP BY CONVERT(DATE,creation_date)";
            queryTwo += " GROUP BY CONVERT(DATE,creation_date)";

            mainQuery += queryOne + " ) AS table_tong\n" +
                    "FULL JOIN\n" +
                    "(" + queryTwo + " ) AS table_thanh_cong\n" +
                    "ON\n" +
                    "table_tong.ngay = table_thanh_cong.ngay";

            List<TanSuatTheoNgay> tanSuatTheoNgays = entityManager.createNativeQuery(mainQuery).getResultList();

            if (tanSuatTheoNgays != null) {
                // convert list data to page data
                int start = pageable.getOffset();
                int end = (start + pageable.getPageSize()) > tanSuatTheoNgays.size()
                        ? tanSuatTheoNgays.size() : (start + pageable.getPageSize());
                tanSuatTheoNgayPage = new PageImpl<TanSuatTheoNgay>(tanSuatTheoNgays.subList(start, end), pageable,
                        tanSuatTheoNgays.size());
            }

            return tanSuatTheoNgayPage;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public Page<HSCongViecLog> getPageSaveLog ( HSCongViecLogSearchObject hsCongViecLogSearchObject, Pageable pageable ) {
        try {
            //Setup query
            Page<HSCongViecLog> page = hsCongViecLogRepository.findAll(HSCongViecLogSpecification.filterSearch(hsCongViecLogSearchObject), pageable);
            return page;
        } catch (Exception ex ) {
            System.err.println("Tim kiem log loi: 2: HSCVLOGservice");
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public Page<HSCongViecLog> getPageUpdateLog ( HSCongViecLogSearchObject hsCongViecLogSearchObject, Pageable pageable ) {
        return null;
    }

    @Override
    public Page<HSCongViecLog> getPageGetLog ( SearchForm searchForm, Pageable pageable ) {
        Page<HSCongViecLog> hsCongViecLogPage = null;
        try {
            hsCongViecLogPage = hsCongViecLogRepository.findAll(HSCongViecLogSpecification.advanceSearch(searchForm), pageable);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return hsCongViecLogPage;
    }


    @Override
    public Page<LogDataDoiSoatDTO> getPageGetLogFollowDate ( HSCongViecDateSearch hsCongViecLogSearchObject, String type, Pageable pageable ) {
        try {
            //Setup query
//            Page<HSCongViecLog> page = hsCongViecLogRepository.findAll(HSCongViecLogSpecification.filterSearchFollowDate(hsCongViecLogSearchObject), pageable);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Page<LogDataDoiSoatDTO> tanSuatTheoNgayPage = null;

            String query = "SELECT id, profile_id, loai, ma_tham_chieu, ma_ht_gui, ma_ht_nhan, type_request, ngay_gui, creation_date, ma_hs_gui, ma_hs_nhan, status_code FROM hscv_log_data WHERE ";
            if(hsCongViecLogSearchObject.getLoaiDoiSoat() != null) {
                if(hsCongViecLogSearchObject.getLoaiDoiSoat().equals("SEND")) {
                    if(type.equals("success"))
                        query += " type_request = 'SAVE' AND status_code != 4";
                    else {
                        query += " type_request = 'SAVE'";
                    }
                    if(hsCongViecLogSearchObject.getHeThong() != null) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            query += " AND ma_ht_gui like '%"+hsCongViecLogSearchObject.getHeThong()+"%'";
                        }
                    }
                } else if(hsCongViecLogSearchObject.getLoaiDoiSoat().equals("RECEIVED")) {
                    if(type.equals("success"))
                        query += " type_request = 'GET' AND status_code != 4";
                    else {
                        query += " type_request = 'GET'";
                    }
                    if(hsCongViecLogSearchObject.getHeThong() != null) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            query += " AND ma_ht_nhan = '%"+hsCongViecLogSearchObject.getHeThong()+"%'";
                        }
                    }
                } else {
                    query += " type_request = ''";
                    if(hsCongViecLogSearchObject.getHeThong() != null) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            query += " AND ma_ht_gui like '%"+hsCongViecLogSearchObject.getHeThong()+"%'";
                        }
                    }
                }
            } else {
                query += " type_request = ''";
            }
            if(hsCongViecLogSearchObject.getLoai() != null) {
                if(!hsCongViecLogSearchObject.getLoai().isEmpty()) {
                    query += " AND loai = '"+hsCongViecLogSearchObject.getLoai()+"'";
                }
            }
            if(hsCongViecLogSearchObject.getNgaytao() != null ){
                query += " AND CONVERT(DATE,creation_date) = '"+formatter.format(hsCongViecLogSearchObject.getNgaytao())+"'";
            }

            if(hsCongViecLogSearchObject.getMa_hs_gui() != null) {
                if(!hsCongViecLogSearchObject.getMa_hs_gui().isEmpty()) {
                    query += " AND ma_hs_gui like '%"+hsCongViecLogSearchObject.getMa_hs_gui()+"%'";
                }
            }

            if(hsCongViecLogSearchObject.getMa_hs_nhan() != null) {
                if(!hsCongViecLogSearchObject.getMa_hs_nhan().isEmpty()) {
                    query += " AND ma_hs_nhan like '%"+hsCongViecLogSearchObject.getMa_hs_nhan()+"%'";
                }
            }

            query += " order by creation_date desc";

            List<LogDataDoiSoatDTO> tanSuatTheoNgays = entityManager.createNativeQuery(query).getResultList();
            if (tanSuatTheoNgays != null) {
                // convert list data to page data
                int start = pageable.getOffset();
                int end = (start + pageable.getPageSize()) > tanSuatTheoNgays.size()
                        ? tanSuatTheoNgays.size() : (start + pageable.getPageSize());
                tanSuatTheoNgayPage = new PageImpl<LogDataDoiSoatDTO>(tanSuatTheoNgays.subList(start, end), pageable,
                        tanSuatTheoNgays.size());
            }
            return tanSuatTheoNgayPage;
        } catch (Exception ex ) {
            System.err.println("Tim kiem log loi: 196 HSCVLOGservice");
            ex.printStackTrace();
            return null;
        }
    }

//    @Override
//    public List<Profile> getListReceiverBySystem( String systemCode) {
//        List<Profile> profileSet = this.entityManager.createQuery("select distinct rs.profile from ProfileReceiveSystem  rs  " +
//                "where rs.tthcSystem.systemCode like ?1 and rs.status not like 'done'")
//                .setParameter(1, systemCode)
//                .getResultList();
//        return profileSet;
//    }
}

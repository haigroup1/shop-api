package com.shop.categories.service.impl;

import com.shop.categories.common.exception.Result;

import com.shop.categories.dao.entity.dto.HSCVSystemSearchDTO;
import com.shop.categories.dao.entity.HSCVSystem;
import com.shop.categories.dao.repository.HSCVSystemRepository;
import com.shop.categories.dao.specifications.HSCVSystemSpecification;
import com.shop.categories.service.HSCVSystemService;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class HSCVSystemServiceImpl implements HSCVSystemService {

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    private HSCVSystemRepository hscvSystemRepository;

    private Logger log = Logger.getLogger(HSCVSystemServiceImpl.class);

    @Override
    public Page<HSCVSystem> getPage( Pageable pageable) {
        Page<HSCVSystem> hscvSystems = null;
        try {
            hscvSystems = hscvSystemRepository.findAll(pageable);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return hscvSystems;
    }

    @Override
    public Page<HSCVSystem> getPage(Pageable pageable, HSCVSystemSearchDTO hscvSystem) {
        Page<HSCVSystem> hscvSystemPage = null;
        try {
            hscvSystemPage = hscvSystemRepository.findAll(HSCVSystemSpecification.advanceSearch(hscvSystem), pageable);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return hscvSystemPage;
    }

    @Override
    public Page<HSCVSystem> getPage(Pageable pageable, String filter) {
        Page<HSCVSystem> hscvSystemPage = null;
        try {
            hscvSystemPage = hscvSystemRepository.findAll(HSCVSystemSpecification.filterSearch(filter), pageable);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return hscvSystemPage;
    }

    @Override
    public List<HSCVSystem> getList() {
        List<HSCVSystem> hscvSystems = null;
        try {
            hscvSystems = hscvSystemRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return hscvSystems;
    }


    @Override
    public HSCVSystem getOne(int id) {
        HSCVSystem hscvSystem = null;
        try {
            hscvSystem = hscvSystemRepository.findById(id);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return hscvSystem;
    }

    @Override
    public Result create(HSCVSystem hscvSystem) {
        try {
            hscvSystemRepository.save(hscvSystem);
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return Result.BAD_REQUEST;
    }

    @Override
    public Result update(HSCVSystem hscvSystem) {
        EntityManager entityManager = emf.createEntityManager();
        try {

            HSCVSystem hscvSystemUpdate = null;
            hscvSystemUpdate = entityManager.find(HSCVSystem.class, hscvSystem.getId());
            if (hscvSystemUpdate != null) {
                hscvSystemUpdate.setMaHT(hscvSystem.getMaHT());
                hscvSystemUpdate.setTenHT(hscvSystem.getTenHT());
                hscvSystemUpdate.setStatus(hscvSystem.getStatus());

                hscvSystemRepository.save(hscvSystemUpdate);
            } else {
                return Result.BAD_REQUEST;
            }
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            return Result.BAD_REQUEST;
        } finally {
            entityManager.close();
        }

    }

    @Override
    public Result delete(int id) {
        try {
            hscvSystemRepository.delete(id);
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return Result.BAD_REQUEST;
    }

    @Override
    @Transactional
    public Result delete(int[] ids) {
        try {
            List<HSCVSystem> hscvSystems = new ArrayList<>();
            for (int i = 0; i < ids.length; i++) {
                hscvSystems.add(this.getOne(ids[i]));
            }
            hscvSystemRepository.delete(hscvSystems);
            return Result.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
        return Result.BAD_REQUEST;
    }
}

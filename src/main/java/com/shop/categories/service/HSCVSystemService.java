package com.shop.categories.service;

import java.util.List;

import com.shop.categories.common.exception.Result;
import com.shop.categories.dao.entity.dto.HSCVSystemSearchDTO;
import com.shop.categories.dao.entity.HSCVSystem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface HSCVSystemService {

    /**
     *
     * @param pageable
     * @return a {@link Page} of api
     */
    Page<HSCVSystem> getPage(Pageable pageable);


    Page<HSCVSystem> getPage(Pageable pageable, HSCVSystemSearchDTO hscvSystem);

    /**
     *
     * @param pageable
     * @param filter: the search keywords
     * @return a {@link Page} of api
     */
    Page<HSCVSystem> getPage(Pageable pageable, String filter);

    /**
     *
     * @return a {@link List} of api
     */
    List<HSCVSystem> getList();


    HSCVSystem getOne(int id);

    Result create(HSCVSystem hscvSystem);

    Result update(HSCVSystem hscvSystem);

    /**
     *
     * @param id: the id of the api
     * @return  a {@link Result}
     */
    Result delete(int id);

    /**
     * delete a list api
     * @param ids: the list id
     * @return a {@link Result}
     */
    Result delete(int[] ids);

}

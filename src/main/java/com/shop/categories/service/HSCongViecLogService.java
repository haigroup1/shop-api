package com.shop.categories.service;

import com.shop.categories.dao.entity.HSCongViecLog;
import com.shop.categories.endpoint.dto.common.SearchForm;
import com.shop.categories.dao.entity.dto.HSCongViecDateSearch;
import com.shop.categories.dao.entity.dto.HSCongViecLogSearchObject;
import com.shop.categories.dao.entity.dto.LogDataDoiSoatDTO;
import com.shop.categories.dao.entity.dto.TanSuatTheoNgay;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface HSCongViecLogService {

    public List<TanSuatTheoNgay> countLog( HSCongViecLogSearchObject hsCongViecLogSearchObject );

    public Page<TanSuatTheoNgay> countLogPage( HSCongViecLogSearchObject hsCongViecLogSearchObject, Pageable pageable );

    public Page<HSCongViecLog> getPageSaveLog( HSCongViecLogSearchObject hsCongViecLogSearchObject, Pageable pageable );

    public Page<HSCongViecLog> getPageUpdateLog( HSCongViecLogSearchObject hsCongViecLogSearchObject, Pageable pageable );

    public Page<HSCongViecLog> getPageGetLog( SearchForm searchForm, Pageable pageable );

    public Page<LogDataDoiSoatDTO> getPageGetLogFollowDate( HSCongViecDateSearch hsCongViecLogSearchObject, String type, Pageable pageable );

}

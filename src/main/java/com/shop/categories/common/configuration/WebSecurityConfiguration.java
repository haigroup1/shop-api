package com.shop.categories.common.configuration;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import com.shop.categories.common.security.TokenAuthenticationFilter;

@Component
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Autowired
    private UserDetailsService userDetailsService;	

	@Value("${web.address}")
	private String webAddress;
	

	@Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public TokenAuthenticationFilter jwtAuthenticationTokenFilter() throws Exception {
        return new TokenAuthenticationFilter();
    }

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.addFilterBefore(jwtAuthenticationTokenFilter(), BasicAuthenticationFilter.class)
        .authorizeRequests()
        
        .antMatchers("/api/savis/categories/api/v1/sso/**").permitAll()
        
        .antMatchers("/api/savis/categories/api/v1/users/**")
//        .hasAnyAuthority("ROLE_ADMIN")
        .permitAll()
        
        .antMatchers(HttpMethod.GET, "/api/savis/categories/api/v1/**")
//        .hasAnyAuthority("ROLE_MEMBER")
        .permitAll()
        
        .antMatchers(HttpMethod.POST, "/api/savis/categories/api/v1/**")
//        .hasAnyAuthority("ROLE_ADMIN")
        .permitAll()
        
        .antMatchers(HttpMethod.PUT, "/api/savis/categories/api/v1/**")
//        .hasAnyAuthority("ROLE_ADMIN")
        .permitAll()
        
        .antMatchers(HttpMethod.DELETE, "/api/savis/categories/api/v1/**")
//        .hasAnyAuthority("ROLE_ADMIN")
        .permitAll()
		.antMatchers(HttpMethod.OPTIONS, "/realtime/1.0/get").permitAll();
//        .anyRequest().authenticated();
		
//		http.sessionManagement().maximumSessions(1);
		
		http.csrf().disable();
//        http.headers().frameOptions().disable();
//        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS);
		http.cors().and();
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(Arrays.asList(webAddress));
		configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE"));
		configuration.setAllowedHeaders(Arrays.asList("Origin", "Access-Control-Allow-Origin",
				"Access-Control-Allow-Headers", "Content-Type", "Authorization", "X-Requested-With"));
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}

	/* To allow Pre-flight [OPTIONS] request from browser */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
		web.ignoring().antMatchers("/savis/categories/api/v1/sso/**");
	}

}

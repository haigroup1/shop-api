package com.shop.categories.common.exception;

import com.shop.categories.common.dto.ResponseMessage;

public class BadRequestException extends RuntimeException {
	
	/* the response message*/
	private ResponseMessage responseMessage;

	private static final long serialVersionUID = 1L;
	
	/**
	 * Instantiates a new BadRequestException entity
	 */
	public BadRequestException(){
		
	}
	
	/**
	 * Instantiates a new BadRequestException entity
	 * @param responseMessage: the response message
	 */
	public BadRequestException(ResponseMessage responseMessage){
		this.responseMessage = responseMessage;
	}

	/* the getter and setter method*/
	public ResponseMessage getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(ResponseMessage responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	
	

}

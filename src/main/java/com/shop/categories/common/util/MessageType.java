package com.shop.categories.common.util;

public enum MessageType {
	SUCCESS, INFO, WARNING, ERROR
}

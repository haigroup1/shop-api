package com.shop.categories.auth.dto.adm.role;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdmRoleDTO {

	private int roleId;
	private String roleName;
	private String loweredRoleName;
	private String roleCode;
	private String description;
	private int enableDelete;
	private int status;
	private int applicationId;
	private int[] admRightIds;
	private int[] admAccessRightIds;
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getLoweredRoleName() {
		return loweredRoleName;
	}
	public void setLoweredRoleName(String loweredRoleName) {
		this.loweredRoleName = loweredRoleName;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getEnableDelete() {
		return enableDelete;
	}
	public void setEnableDelete(int enableDelete) {
		this.enableDelete = enableDelete;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public int[] getAdmRightIds() {
		return admRightIds;
	}
	public void setAdmRightIds(int[] admRightIds) {
		this.admRightIds = admRightIds;
	}
	public int[] getAdmAccessRightIds() {
		return admAccessRightIds;
	}
	public void setAdmAccessRightIds(int[] admAccessRightIds) {
		this.admAccessRightIds = admAccessRightIds;
	}
	
	

}

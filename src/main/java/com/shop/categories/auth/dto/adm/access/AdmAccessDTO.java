package com.shop.categories.auth.dto.adm.access;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdmAccessDTO {
	private int accessId;
	private String accessType;
	private String accessName;
	private String accessKey;
	private int viewOrder;
	private int[] admApiIds;
	public int getAccessId() {
		return accessId;
	}
	public void setAccessId(int accessId) {
		this.accessId = accessId;
	}
	public String getAccessType() {
		return accessType;
	}
	public void setAccessType(String accessType) {
		this.accessType = accessType;
	}
	public String getAccessName() {
		return accessName;
	}
	public void setAccessName(String accessName) {
		this.accessName = accessName;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public int getViewOrder() {
		return viewOrder;
	}
	public void setViewOrder(int viewOrder) {
		this.viewOrder = viewOrder;
	}
	public int[] getAdmApiIds() {
		return admApiIds;
	}
	public void setAdmApiIds(int[] admApiIds) {
		this.admApiIds = admApiIds;
	}
	
	

}

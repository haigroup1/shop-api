package com.shop.categories.auth.dto.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shop.categories.common.exception.Result;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Response data form
 * 
 * @author LongTT
 *
 * @param <T>
 */

@Getter
@Setter
@AllArgsConstructor
public class ResponseData<T> {

	/** the response code */
	private int code;

	/** the response message */
	private String message;

	/** the response data */
	private T Data;

	/**
	 * constructor
	 * 
	 * @param result
	 *            the result
	 */
	public ResponseData(Result result) {
		this.code = result.getCode();
		this.message = result.getMessage();
	}

	/**
	 * constructor
	 * 
	 * @param data
	 *            the data
	 * @param result
	 *            the result
	 */
	public ResponseData(T data, Result result) {
		this.Data = data;
		this.code = result.getCode();
		this.message = result.getMessage();
	}

	/**
	 * check response status is success
	 * @return 
	 */
	@JsonIgnore
	public boolean isSucess() {
		return (this.code == 1);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return Data;
	}

	public void setData(T data) {
		Data = data;
	}

	public ResponseData() {
		super();
	}
	
	
	

}

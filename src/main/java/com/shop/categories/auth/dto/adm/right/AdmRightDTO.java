package com.shop.categories.auth.dto.adm.right;

import java.util.List;

import com.shop.categories.auth.dto.adm.access.AdmAccessDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdmRightDTO {

	private int rightId;
	private int parentRightId;
	private String rightCode;
	private String rightName;
	private int status;
	private int rightOrder;
	private int hasChild;
	private String urlRewrite;
	private String iconUrl;
	private String description;
	private int applicationId;

	private int[] admApiIds;
	private List<AdmAccessDTO> listAdmAccess;
	public int getRightId() {
		return rightId;
	}
	public void setRightId(int rightId) {
		this.rightId = rightId;
	}
	public int getParentRightId() {
		return parentRightId;
	}
	public void setParentRightId(int parentRightId) {
		this.parentRightId = parentRightId;
	}
	public String getRightCode() {
		return rightCode;
	}
	public void setRightCode(String rightCode) {
		this.rightCode = rightCode;
	}
	public String getRightName() {
		return rightName;
	}
	public void setRightName(String rightName) {
		this.rightName = rightName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getRightOrder() {
		return rightOrder;
	}
	public void setRightOrder(int rightOrder) {
		this.rightOrder = rightOrder;
	}
	public int getHasChild() {
		return hasChild;
	}
	public void setHasChild(int hasChild) {
		this.hasChild = hasChild;
	}
	public String getUrlRewrite() {
		return urlRewrite;
	}
	public void setUrlRewrite(String urlRewrite) {
		this.urlRewrite = urlRewrite;
	}
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public int[] getAdmApiIds() {
		return admApiIds;
	}
	public void setAdmApiIds(int[] admApiIds) {
		this.admApiIds = admApiIds;
	}
	public List<AdmAccessDTO> getListAdmAccess() {
		return listAdmAccess;
	}
	public void setListAdmAccess(List<AdmAccessDTO> listAdmAccess) {
		this.listAdmAccess = listAdmAccess;
	}
	
	

}

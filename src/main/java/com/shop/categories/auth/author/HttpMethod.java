package com.shop.categories.auth.author;

public enum HttpMethod {

	/** The get method. */
	GET("GET", "GET"),

	/** The post method. */
	POST("POST", "POST"),

	/** The put method. */
	PUT("PUT", "PUT"),

	/** The delete method. */
	DELETE("DELETE", "DELETE");

	/** The value. */
	public String value;

	/** The description. */
	public String description;

	/**
	 * Instantiates a new api version state.
	 *
	 * @param value
	 *            the value
	 * @param description
	 *            the description
	 */
	private HttpMethod(String value, String description) {
		this.value = value;
		this.description = description;
	}

}

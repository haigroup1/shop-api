package com.shop.categories.auth.author;

public interface AuthorizationService {

	boolean isAuthorizaed(String username, String url, String method);

	String getUserInfo(String username);

}

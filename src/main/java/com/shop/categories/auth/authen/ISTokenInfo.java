package com.shop.categories.auth.authen;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ISTokenInfo {

	@SerializedName("access_token")
	@Expose
	private String accessToken;
	@SerializedName("refresh_token")
	@Expose
	private String refreshToken;
	@SerializedName("scope")
	@Expose
	private String scope;
	@SerializedName("id_token")
	@Expose
	private String idToken;
	@SerializedName("token_type")
	@Expose
	private String tokenType;
	@SerializedName("expires_in")
	@Expose
	private int expiresIn;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getIdToken() {
		return idToken;
	}

	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public int getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}

}

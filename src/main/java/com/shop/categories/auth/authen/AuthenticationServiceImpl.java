package com.shop.categories.auth.authen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.shop.categories.common.constants.CommonConstants;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	@Value("${sso.client_id}")
	private String clientId;

	@Value("${sso.secret_id}")
	private String secretId;

	@Value("${sso.redirect_uri}")
	private String redirectUri;

	@Value("${sso.tokenUrl}")
	private String tokenUrl;

	@Value("${sso.getUsernameUrl}")
	private String getUsernameUrl;

	@Override
	public boolean isAuthenticated(String accessToken) {
		ISUserInfo userInfo = this.getUserInfo(accessToken);
		if (userInfo != null) {
			return true;
		}
		return false;
	}

	@Override
	public ISUserInfo getUserInfo(String accessToken) {
		ISUserInfo userInfo = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(getUsernameUrl);
			post.setHeader(CommonConstants.HEADER_FIELD.AUTHORIZATION, "Bearer " + accessToken);

			HttpResponse httpResponse;
			httpResponse = client.execute(post);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			String jsonData = EntityUtils.toString(httpResponse.getEntity());
			if (statusCode == 200) {
				Gson gson = new Gson();
				userInfo = gson.fromJson(jsonData, ISUserInfo.class);
			}

		} catch (ClientProtocolException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return userInfo;
	}

	@Override
	public ISTokenInfo getAccessTokenByCode(String authorizationCode) {
		try {

			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(tokenUrl);

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("client_id", clientId));
			urlParameters.add(new BasicNameValuePair("client_secret", secretId));
			urlParameters.add(new BasicNameValuePair("grant_type", "authorization_code"));
			urlParameters.add(new BasicNameValuePair("code", authorizationCode));
			urlParameters.add(new BasicNameValuePair("redirect_uri", redirectUri));

			post.setEntity(new UrlEncodedFormEntity(urlParameters));

			HttpResponse response = client.execute(post);
			int statusCode = response.getStatusLine().getStatusCode();
			String jsonData = EntityUtils.toString(response.getEntity());
			System.out.println(jsonData);
			if (statusCode == 200) {
				Gson gson = new Gson();
				ISTokenInfo tokenInfo = gson.fromJson(jsonData, ISTokenInfo.class);
				return tokenInfo;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ISTokenInfo getAccessTokenByRefreshToken(String refreshToken) {
		try {

			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(tokenUrl);

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("grant_type", "refresh_token"));
			urlParameters.add(new BasicNameValuePair("refresh_token", refreshToken));

			Base64.Encoder encoder = Base64.getEncoder();
			post.setHeader("Authorization", "Basic " + encoder.encodeToString((clientId + ":" + secretId).getBytes()));
			post.setHeader("Content-Type", "application/x-www-form-urlencoded");
			post.setEntity(new UrlEncodedFormEntity(urlParameters));

			HttpResponse response = client.execute(post);
			int statusCode = response.getStatusLine().getStatusCode();
			String jsonData = EntityUtils.toString(response.getEntity());
			System.out.println(jsonData);
			if (statusCode == 200) {
				Gson gson = new Gson();
				ISTokenInfo tokenInfo = gson.fromJson(jsonData, ISTokenInfo.class);
				return tokenInfo;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}

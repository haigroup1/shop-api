package com.shop.categories.dao.specifications;

import com.shop.categories.dao.entity.dto.HSCVSystemSearchDTO;
import com.shop.categories.dao.entity.HSCVSystem;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class HSCVSystemSpecification {
    public static Specification<HSCVSystem> filterSearch(String filter) {
        return new Specification<HSCVSystem>() {

            @Override
            public Predicate toPredicate(Root<HSCVSystem> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicateList = new ArrayList<>();
                Predicate obj = null;

                if (filter != null) {
                    //search in api name column
                    obj = cb.like(cb.lower(root.get("maHT")), "%" + filter.toLowerCase() + "%");
                    predicateList.add(obj);

                    //search in api method column
                    obj = cb.like(cb.lower(root.get("tenHT")), "%" + filter.toLowerCase() + "%");
                    predicateList.add(obj);
                }

                return cb.or(predicateList.toArray(new Predicate[predicateList.size()]));
            }
        };
    }

    public static Specification<HSCVSystem> advanceSearch( HSCVSystemSearchDTO hscvSystem) {
        return new Specification<HSCVSystem>() {

            @Override
            public Predicate toPredicate(Root<HSCVSystem> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<>();
                Predicate obj = null;
                // search in api name column
                if (hscvSystem.getMaHT() != null && hscvSystem.getMaHT() != "") {
                    obj = cb.like(cb.lower(root.get("maHT")), "%" + hscvSystem.getMaHT().toLowerCase() + "%");
                    predicateList.add(obj);
                }
                if (hscvSystem.getTenHT() != null && hscvSystem.getTenHT() != "") {
                    obj = cb.like(cb.lower(root.get("tenHT")), "%" + hscvSystem.getTenHT().toLowerCase() + "%");
                    predicateList.add(obj);
                }
                if (hscvSystem.getStatus() != 2) {
                    obj = cb.equal(root.get("status"),hscvSystem.getStatus());
                    predicateList.add(obj);
                }

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
            }
        };
    }
}

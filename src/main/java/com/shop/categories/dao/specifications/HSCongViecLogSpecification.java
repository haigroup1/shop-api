package com.shop.categories.dao.specifications;

import com.shop.categories.dao.entity.HSCongViecLog;
import com.shop.categories.dao.entity.dto.HSCongViecLogSearchObject;
import com.shop.categories.endpoint.dto.common.SearchForm;
import com.shop.categories.dao.entity.dto.HSCongViecDateSearch;
// import HSCongViecLogSearchObject;
import lombok.SneakyThrows;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class HSCongViecLogSpecification {

    public static Specification<HSCongViecLog> filterSearch( HSCongViecLogSearchObject hsCongViecLogSearchObject) {
        return new Specification<HSCongViecLog>() {

            @Override
            public Predicate toPredicate( Root<HSCongViecLog> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicateList = new ArrayList<>();
                Predicate obj = null;

                if (hsCongViecLogSearchObject.getLoai() != null ) {
                    if(!hsCongViecLogSearchObject.getLoai().isEmpty()) {
                        obj = cb.equal(cb.lower(root.get("loai")), hsCongViecLogSearchObject.getLoai());
                        predicateList.add(obj);
                    }
                }

                if (hsCongViecLogSearchObject.getFromDate() != null) {
                    obj = cb.greaterThanOrEqualTo(root.get("ngayGui"), hsCongViecLogSearchObject.getFromDate());
                    predicateList.add(obj);
                }
                if (hsCongViecLogSearchObject.getToDate() != null) {
                    obj = cb.greaterThanOrEqualTo(root.get("ngayGui"), hsCongViecLogSearchObject.getToDate());
                    predicateList.add(obj);
                }

                if(hsCongViecLogSearchObject.getLoaiDoiSoat().equals("SEND")) {
                    if(hsCongViecLogSearchObject.getHeThong() != null ) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            obj = cb.like(cb.lower(root.get("maHTGui")), "%" + hsCongViecLogSearchObject.getHeThong() + "%");
                            predicateList.add(obj);
                        }
                    }

                    obj = cb.equal(root.get("type_request"), "SAVE");
                    predicateList.add(obj);

                } else {
                    if(hsCongViecLogSearchObject.getHeThong() != null) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            obj = cb.like(cb.lower(root.get("maHTNhan")), "%" + hsCongViecLogSearchObject.getHeThong() + "%");
                            predicateList.add(obj);
                        }
                    }

                    obj = cb.equal(root.get("type_request"), "GET");
                    predicateList.add(obj);
                }

                query.orderBy(cb.desc(root.get("creation_date")));

                return cb.or(predicateList.toArray(new Predicate[predicateList.size()]));
            }
        };
    }

    public static Specification<HSCongViecLog> filterSearchFollowDate( HSCongViecDateSearch hsCongViecLogSearchObject) {
        return new Specification<HSCongViecLog>() {

            @SneakyThrows
            @Override
            public Predicate toPredicate( Root<HSCongViecLog> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                List<Predicate> predicateList = new ArrayList<>();
                Predicate obj = null;

                if (hsCongViecLogSearchObject.getLoai() != null ) {
                    if(!hsCongViecLogSearchObject.getLoai().isEmpty()) {
                        obj = cb.equal(cb.lower(root.get("loai")), hsCongViecLogSearchObject.getLoai());
                        predicateList.add(obj);
                    }
                }

//                if (hsCongViecLogSearchObject.getNgaytao() != null) {
//                    String date = sdf.format(hsCongViecLogSearchObject.getNgaytao());
//                    Date searchDate = new SimpleDateFormat("dd/MM/yyyy").parse(date);
                    obj = cb.equal(root.get("creation_date"), hsCongViecLogSearchObject.getNgaytao());
                    predicateList.add(obj);
//                    obj = cb.lessThanOrEqualTo(root.get("creation_date"), hsCongViecLogSearchObject.getNgaytao());
//                    predicateList.add(obj);
//                }

                if(hsCongViecLogSearchObject.getLoaiDoiSoat().equals("SEND")) {
                    if(hsCongViecLogSearchObject.getHeThong() != null ) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            obj = cb.like(cb.lower(root.get("maHTGui")), "%" + hsCongViecLogSearchObject.getHeThong() + "%");
                            predicateList.add(obj);
                        }
                    }

                    obj = cb.equal(root.get("type_request"), "SAVE");
                    predicateList.add(obj);

                } else if(hsCongViecLogSearchObject.getLoaiDoiSoat().equals("RECEIVED")) {
                    if(hsCongViecLogSearchObject.getHeThong() != null) {
                        if(!hsCongViecLogSearchObject.getHeThong().isEmpty()) {
                            obj = cb.like(cb.lower(root.get("maHTNhan")), "%" + hsCongViecLogSearchObject.getHeThong() + "%");
                            predicateList.add(obj);
                        }
                    }

                    obj = cb.equal(root.get("type_request"), "GET");
                    predicateList.add(obj);
                } else {
                    obj = cb.equal(root.get("type_request"), "");
                    predicateList.add(obj);
                }

                query.orderBy(cb.desc(root.get("creation_date")));

                return cb.or(predicateList.toArray(new Predicate[predicateList.size()]));
            }
        };
    }

    public static Specification<HSCongViecLog> advanceSearch( SearchForm searchForm) {
        return new Specification<HSCongViecLog>() {

            @Override
            public Predicate toPredicate(Root<HSCongViecLog> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<>();
                Predicate obj = null;
                // search in api name column
                if (searchForm.getLoai() != null ) {
                    if(!searchForm.getLoai().isEmpty()) {
                        obj = cb.equal(root.get("loai"), searchForm.getLoai());
                        predicateList.add(obj);
                    }
                }

                if (searchForm.getMaHTGui() != null ) {
                    if(!searchForm.getMaHTGui().isEmpty()) {
                        obj = cb.like(cb.lower(root.get("maHTGui")), "%"+searchForm.getMaHTGui()+"%");
                        predicateList.add(obj);
                    }
                }

                if (searchForm.getMaHTNhan() != null ) {
                    if(!searchForm.getMaHTNhan().isEmpty()) {
                        obj = cb.like(cb.lower(root.get("maHTNhan")), "%"+searchForm.getMaHTNhan()+"%");
                        predicateList.add(obj);
                    }
                }

                if (searchForm.getType_request() != null ) {
                    if(!searchForm.getType_request().isEmpty()) {
                        obj = cb.equal(root.get("type_request"), searchForm.getType_request());
                        predicateList.add(obj);
                    }
                }

                if (searchForm.getFromDate() != null) {
                    obj = cb.greaterThanOrEqualTo(root.get("creation_date"), searchForm.getFromDate());
                    predicateList.add(obj);
                }
                if (searchForm.getToDate() != null) {
                    obj = cb.lessThanOrEqualTo(root.get("creation_date"), searchForm.getToDate());
                    predicateList.add(obj);
                }

                if (searchForm.getMaHSGui() != null ) {
                    if(!searchForm.getMaHSGui().isEmpty()) {
                        obj = cb.like(cb.lower(root.get("maHSGui")), "%"+searchForm.getMaHSGui()+"%");
                        predicateList.add(obj);
                    }
                }

                if (searchForm.getMaHSNhan() != null ) {
                    if(!searchForm.getMaHSNhan().isEmpty()) {
                        obj = cb.like(cb.lower(root.get("maHSNhan")), "%"+searchForm.getMaHSNhan()+"%");
                        predicateList.add(obj);
                    }
                }

                query.orderBy(cb.desc(root.get("creation_date")));

                return cb.and(predicateList.toArray(new Predicate[predicateList.size()]));
            }
        };
    }
}

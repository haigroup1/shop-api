package com.shop.categories.dao.entity.view;

import com.shop.categories.common.util.DateUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogDataView {

    @Column(name = "count_total")
    private Long count_total;
    @Column(name = "count_success")
    private Long count_success;
    @Id
    @Column(name = "creation_date_sub")
    private String creation_date_sub;

    @Transient
    private Date creation_date;
    @Transient
    private String type_detail;
    @Transient
    private String creation_date_str;
    @Transient
    private String creation_date_sub_str;

    public String getCreation_date_sub_str() {
        if(creation_date_sub != null) {
            creation_date = DateUtils.stringToDate(creation_date_sub, "yyyyMMdd");
            creation_date_sub_str = DateUtils.date2str(creation_date, "dd/MM/yyyy");
        }
        return creation_date_sub_str;
    }

    public Date getCreation_date() {
        if(creation_date_sub != null)
            creation_date = DateUtils.stringToDate(creation_date_sub, "yyyyMMdd");
        return creation_date;
    }

    public String getCreation_date_str() {
        if(creation_date_sub != null) {
            creation_date = DateUtils.stringToDate(creation_date_sub, "yyyyMMdd");
            creation_date_str = DateUtils.date2str(creation_date, "dd/MM/yyyy HH:mm:ss");
        }
        return creation_date_str;
    }

}

package com.shop.categories.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name="hscv_log_data")
@NoArgsConstructor
@AllArgsConstructor
public class HSCongViecLog {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "profile_id")
    private Integer profileId;

    @Column(name = "loai")
    private String loai;

    @Column(name = "ma_tham_chieu")
    private String maThamChieu;

    @Column(name = "ma_ht_gui")
    private String maHTGui;

    @Column(name = "ma_ht_nhan")
    private String maHTNhan;

    @Column(name = "status_code")
    private Integer status_code;

    @Column(name = "type_request")
    private String type_request;

    @Column(name = "ngay_gui")
    private Date ngayGui;

    @Column(name = "creation_date")
    private Date creation_date;

    @Column(name = "data_input")
    private String data_input;

    @Column(name = "data_output")
    private String data_output;

    @Column(name = "ma_hs_gui")
    private String maHSGui	;

    @Column(name = "ma_hs_nhan")
    private String maHSNhan	;


}

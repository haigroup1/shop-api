package com.shop.categories.dao.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HSCongViecLogSearchDTO {
    private String loai;
    private String maHTGui;
    private String maHTNhan;
    private String type_request;
    private Date fromDate;
    private Date toDate;
    private String maHSGui;
    private String maHSNhan;
}

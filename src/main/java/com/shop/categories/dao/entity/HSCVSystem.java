package com.shop.categories.dao.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "hethongguinhan")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HSCVSystem {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name="ma_ht")
    private String maHT;

    @Column(name="ten_ht")
    private String tenHT;

    @Column(name = "status")
    private Integer status;
}

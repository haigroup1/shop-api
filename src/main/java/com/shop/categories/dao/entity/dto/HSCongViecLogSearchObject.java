package com.shop.categories.dao.entity.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.internal.util.IgnoreJava6Requirement;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HSCongViecLogSearchObject {
    private String loai;
    private String heThong;
    private Date fromDate;
    private Date toDate;
    private String loaiDoiSoat;
    private String ma_hs_gui;
    private String ma_hs_nhan;
}

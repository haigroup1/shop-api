package com.shop.categories.dao.entity.view;

import com.shop.categories.auth.dto.common.JsonHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HoSoCVLogDataView implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    private Long id;
    @Column(name = "status")
    private String status;
    @Column(name = "type")
    private String type;
    @Column(name = "code")
    private String code;
    @Column(name = "system_id")
    private String system_id;
    @Column(name = "creation_date")
    private Date creation_date;
    @Column(name = "ref_code")
    private String ref_code;
    @Column(name = "api_path")
    private String api_path;
    @Column(name = "unit_code")
    private String unit_code;
    @Column(name = "data_input")
    private String data_input;
    @Column(name = "data_output")
    private String data_output;
    @Column(name = "profile_id")
    private Long profile_id;
    @Column(name = "source_id")
    private String source_id;
    @Column(name = "MaHSNhan")
    private String maHSNhan;

    @Transient
    private Object data_input_o;
    @Transient
    private Object data_output_o;

    public Object getData_input_o() {
        if (StringUtils.isNotBlank(data_input)) {
            data_input_o = JsonHelper.jsonToObject(data_input, Object.class);
        }
        return data_input_o;
    }

    public Object getData_output_o() {
        if (StringUtils.isNotBlank(data_output)) {
            data_output_o = JsonHelper.jsonToObject(data_output, Object.class);
        }
        return data_output_o;
    }
}

package com.shop.categories.dao.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TanSuatTheoNgay {

    @Id
    @Column(name = "ngay")
    private Date ngay;
    @Column(name = "so_luong")
    private Integer so_luong;
    @Column(name = "so_luong_tc")
    private Integer so_luong_tc;
}

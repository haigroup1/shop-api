package com.shop.categories.dao.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HSCVSystemSearchDTO {
//    @Access(AccessType.FIELD)
    private int id;

    private String maHT;

    private String tenHT;

    private int status;
}

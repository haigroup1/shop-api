package com.shop.categories.dao.repository;

import com.shop.categories.dao.entity.HSCVLogData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface HSCVLogDataRepository extends JpaRepository<HSCVLogData, Long>, JpaSpecificationExecutor<HSCVLogData> {
}

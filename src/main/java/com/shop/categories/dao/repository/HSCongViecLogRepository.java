package com.shop.categories.dao.repository;

import com.shop.categories.dao.entity.HSCongViecLog;
import com.shop.categories.dao.entity.dto.TanSuatTheoNgay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface HSCongViecLogRepository extends JpaRepository<HSCongViecLog, Integer>, JpaSpecificationExecutor<HSCongViecLog> {

    @Query(value = "select CONVERT(DATE,creation_date) as ngay, COUNT(profile_id) as so_luong_tc " +
            "from hscv_log_data WHERE type_request = :type_request " +
            "GROUP BY CONVERT(DATE,creation_date)", nativeQuery = true)
    TanSuatTheoNgay countRequestFollowDate( @Param("type_request") String type_request);
}

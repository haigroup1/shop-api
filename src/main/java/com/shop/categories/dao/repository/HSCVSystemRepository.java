package com.shop.categories.dao.repository;

import com.shop.categories.dao.entity.HSCVSystem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface HSCVSystemRepository extends JpaRepository<HSCVSystem, Integer>, JpaSpecificationExecutor<HSCVSystem> {

    HSCVSystem findById(int id);
}
